from django.apps import AppConfig


class DiaphragmConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'diaphragm'
