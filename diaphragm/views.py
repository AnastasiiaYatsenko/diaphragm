from django.shortcuts import render, redirect
from django.http import HttpResponse
from diaphragm.models import Photographer, Photo, Collection, Comment, PhotoCollection
from diaphragm.forms import AddPhotoForm, LoginForm, RegisterForm, CommentForm, AddToCollectionForm, AddCollectionForm
from django.views.generic import ListView, CreateView, DetailView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


class HomeListView(ListView):
    model = Photo
    template_name = 'home.html'
    context_object_name = 'photo_list'

    def get_queryset(self):
        return Photo.objects.all().order_by('-likes_num')


class DetailedPhotoView(LoginRequiredMixin, DetailView):
    login_url = '/login'

    def get_context_data(self, **kwargs):
        photo_id = self.kwargs['photo_id']
        photo = Photo.objects.get(pk=photo_id)
        context = {
            'photo': photo,
            'comments': Comment.objects.filter(photo=photo),
            'collections': Collection.objects.filter(owner=(self.request.user.pk-1))
        }
        return context

    def get(self, request, **kwargs):
        form_collections = AddToCollectionForm()
        form_collections.set_user(self.request.user.pk-1)
        form_comment = CommentForm()
        context = self.get_context_data(**kwargs)
        context['col_form'] = form_collections
        context['com_form'] = form_comment
        return render(request, 'detailed.html', context=context)

    def post(self, request, **kwargs):
        form_collections = AddToCollectionForm(request.POST)
        form_comment = CommentForm(request.POST)
        photo_id = self.kwargs['photo_id']
        # add comment or collection
        context = self.get_context_data(**kwargs)
        return render(request, 'detailed.html', context=context)


class CollectionView(LoginRequiredMixin, DetailView):
    login_url = '/login'

    def get_context_data(self, **kwargs):
        collection_id = self.kwargs['collection_id']
        collection = Collection.objects.get(pk=collection_id)
        context = {
            'collection': collection,
            'photo_list': Photo.objects.filter(collection=collection)
        }
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)

        return render(request, 'collection.html', context=context)


class CollectionListView(LoginRequiredMixin, ListView):
    model = Collection
    template_name = 'collections_list.html'
    context_object_name = 'collections_list'
    login_url = '/login'

    def get_queryset(self):
        return Collection.objects.filter(owner=(self.request.user.pk-1))


class ProfileView(LoginRequiredMixin, DetailView):
    model = Collection
    template_name = 'collections_list.html'
    context_object_name = 'collections_list'
    login_url = '/login'

    def get_context_data(self, **kwargs):
        user = Photographer.objects.filter(pk=(self.request.user.pk-1))[0]
        photos = Photo.objects.filter(photographer=user)
        context = {
            'photo_list': photos,
            'user': user
        }
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, 'profile.html', context=context)


class AddPhotoView(LoginRequiredMixin, CreateView):
    login_url = '/login'

    def get(self, request, **kwargs):
        form = AddPhotoForm()
        return render(request, 'add_photo.html')

    def post(self, request, **kwargs):
        form = AddPhotoForm(request.POST, request.FILES)

        return render(request, 'add_photo.html')


class AddCollectionView(LoginRequiredMixin, CreateView):
    login_url = '/login'

    def get(self, request, **kwargs):
        form = AddCollectionForm()
        context = {
            'form': form
        }
        return render(request, 'add_collection.html', context=context)

    def post(self, request, **kwargs):
        form = AddCollectionForm(request.POST)
        context = {
            'form': form
        }
        return render(request, 'add_collection.html', context=context)


def like(request, **kwargs):
    # like view
    return redirect('/')


def del_photo(request, **kwargs):
    # delete photo view
    return redirect('/')


def del_comment(request, **kwargs):
    # delete comment view
    return redirect('/')


def add_to_col(request, **kwargs):
    # add to collection view
    return redirect('/')


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
            if user is not None and user.is_active:
                login(request, user)
                return redirect('/')
        else:
            return render(request, 'login.html', {'form': form})
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('/login')


def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = User.objects.create_user(username=username, password=password)
            ph = Photographer(user=user)
            ph.save()
            login(request, user)
            return redirect('/')
        else:
            return render(request, 'registration.html', {'form': form})
    else:
        form = RegisterForm()
    return render(request, 'registration.html', {'form': form})
