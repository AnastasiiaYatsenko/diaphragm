from diaphragm.models import Photographer, Photo, Collection, Comment
from django import forms


class AddPhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ('photo',)


class AddToCollectionForm(forms.Form):
    def set_user(self, u):
        self.fields['select_collection'].queryset = Collection.objects.filter(owner=u)
    select_collection = forms.ChoiceField()


class AddCollectionForm(forms.ModelForm):
    class Meta:
        model = Collection
        fields = ('name',)
    name = forms.CharField()


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)
    text = forms.TextInput()


class LoginForm(forms.Form):
    username = forms.CharField(max_length=120)
    password = forms.CharField(max_length=120,
                               widget=forms.PasswordInput)


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=120)
    password = forms.CharField(max_length=120,
                               widget=forms.PasswordInput)
    confirm = forms.CharField(max_length=120,
                              widget=forms.PasswordInput)

    def clean(self):
        password = self.cleaned_data['password']
        confirm = self.cleaned_data['confirm']
        if password != confirm:
            raise forms.ValidationError('Passwords are not equal')
        return self.cleaned_data
