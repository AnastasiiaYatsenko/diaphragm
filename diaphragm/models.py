from datetime import datetime
from django.db import models
from django.contrib.auth.models import User


class Photographer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField()
    objects = models.Manager()


class Photo(models.Model):
    photo = models.ImageField()
    date_of_publication = models.DateTimeField(default=datetime.now())
    likes_num = models.IntegerField(default=0)
    photographer = models.ForeignKey(Photographer, on_delete=models.CASCADE)
    objects = models.Manager()


class Collection(models.Model):
    name = models.CharField(max_length=60)
    photos = models.ManyToManyField(Photo, through='PhotoCollection')
    owner = models.ForeignKey(Photographer, on_delete=models.CASCADE)
    cover = models.ImageField(default=None, blank=True)
    objects = models.Manager()


class PhotoCollection(models.Model):
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    date_added = models.DateField(default=datetime.now())


class Comment(models.Model):
    user = models.ForeignKey(Photographer, on_delete=models.CASCADE)
    text = models.TextField()
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    objects = models.Manager()


class Message(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='author')
    to = models.ForeignKey(User, on_delete=models.CASCADE, related_name='to')
    text = models.TextField()
    datetime = models.DateTimeField(default=datetime.now())
    objects = models.Manager()
    is_read = models.BooleanField(default=False)
