from django.db.models import Count, Q
from django.shortcuts import render, redirect
from django.http import HttpResponse
from diaphragm.models import Photographer, Photo, Collection, Comment, PhotoCollection, Message
from diaphragm.forms import AddPhotoForm, LoginForm, RegisterForm, CommentForm, AddToCollectionForm, AddCollectionForm
from django.views.generic import ListView, CreateView, DetailView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


class UserChatView(LoginRequiredMixin, DetailView):
    login_url = '/login'

    def get_context_data(self, **kwargs):
        chat = Message.objects.filter(author__in=[self.request.user, 1],
                                      to__in=[self.request.user, 1]).order_by('datetime')
        context = {
            'chats': chat,
        }
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, 'user_chat.html', context=context)


class AdminListView(LoginRequiredMixin, DetailView):
    login_url = '/login'

    def get_context_data(self, **kwargs):
        chats = Photographer.objects.all()
        #chats = Message.objects.all().distinct('author')
        print(chats)
        context = {
            'chats': chats,
        }
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, 'chat_list.html', context=context)


class AdminChatView(LoginRequiredMixin, DetailView):
    login_url = '/login'

    def get_context_data(self, **kwargs):
        user_id = int(self.kwargs['user_id'])+1
        chat = Message.objects.filter(author__in=[user_id, 1],
                                      to__in=[user_id, 1]).order_by('datetime')
        context = {
            'chats': chat,
        }
        return context

    def get(self, request, **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, 'admin_chat.html', context=context)

    def get_queryset(self):
        return Message.objects.filter(author__pk__in=[self.request.user.pk-1, 1]).order_by('datetime')
