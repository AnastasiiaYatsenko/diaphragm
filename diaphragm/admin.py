from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
import diaphragm.models


class PhotoCollectionInline(admin.TabularInline):
    model = diaphragm.models.Collection.photos.through


class CollectionAdmin(admin.ModelAdmin):
    inlines = [PhotoCollectionInline]


class PhotoAdmin(admin.ModelAdmin):
    inlines = (PhotoCollectionInline,)


class PhotographerInline(admin.StackedInline):
    model = diaphragm.models.Photographer
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (PhotographerInline,)


# class MessageAdmin(admin.ModelAdmin):
#     model = diaphragm.models.Message


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(diaphragm.models.Photo, PhotoAdmin)
admin.site.register(diaphragm.models.Collection, CollectionAdmin)
admin.site.register(diaphragm.models.Photographer)
admin.site.register(diaphragm.models.Comment)
admin.site.register(diaphragm.models.Message)
