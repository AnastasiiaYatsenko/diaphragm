from django.contrib import admin
from django.urls import path
from diaphragm import views, view_chats
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomeListView.as_view(), name='index'),
    path('detail_<photo_id>', views.DetailedPhotoView.as_view(), name='detail'),
    path('collections', views.CollectionListView.as_view(), name='collections'),
    path('collection_<collection_id>', views.CollectionView.as_view(), name='collection'),
    path('add', views.AddPhotoView.as_view(), name='add'),
    path('profile', views.ProfileView.as_view(), name='profile'),
    path('like_<photo_id>', views.like),
    path('del_photo_<photo_id>', views.del_photo),
    path('del_comment_<comment_id>', views.del_comment),
    path('add_collection_with_<photo_id>', views.AddCollectionView.as_view()),
    path('chat', view_chats.UserChatView.as_view()),
    path('chat_list', view_chats.AdminListView.as_view()),
    path('chat_with_<user_id>', view_chats.AdminChatView.as_view()),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register_view, name='register'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
